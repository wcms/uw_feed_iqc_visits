<?php

/**
 * @file
 * uw_feed_iqc_visits.features.inc
 */

/**
 * Implements hook_views_api().
 */
function uw_feed_iqc_visits_views_api() {
  return array("api" => "3.0");
}
